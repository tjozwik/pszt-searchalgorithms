require 'rbtree'

class FileManager
  def initialize(map)
    @map = map
  end

  def import_points(file_path)
    File.open(file_path, 'r').each_line { |l| @map.add_point(*l.split.map(&:to_i)) }
  end

  def export_path(points_path, file_path)
    file = File.new(file_path, 'w')
    points_path.each { |p| file.puts "#{p.x} #{p.y}" }
  end
end

class Point
  attr_reader :x, :y, :map

  def initialize(x, y, map = nil)
    @x = x
    @y = y
    @map = map
  end
end

class Map
  attr_reader :points

  def initialize
    @points = []
  end

  def add_point(x, y)
    point = Point.new(x, y, self)

    @points << point
  end

  def distance(first_point, second_point)
    Math.sqrt((first_point.x - second_point.x) ** 2 + (first_point.y - second_point.y) ** 2)
  end
end

class TreeNode
  attr_reader :point, :distance, :neighbours, :cost, :current_path

  def initialize(point, distance, neighbours, current_path)
    @point = point
    @distance = distance
    @neighbours = neighbours
    @current_path = current_path << point
    if neighbours.empty?
      @cost = distance + point.map.distance(point, point.map.points.first)
    else
      @cost = distance + heuristic_value
    end
  end

  private

  def heuristic_value
    # using Prim's algorithm
    mst_distance = 0
    possible_steps = []

    @neighbours.each do |p|
      possible_steps << [@point.map.distance(@point, p), p]
    end

    if @point != @point.map.points.first
      possible_steps << [
          @point.map.distance(@point, @point.map.points.first) + (possible_steps.max_by { |e| e.first }).first,
          @point.map.points.first
      ]
    end

    current_point = @point

    until possible_steps.empty?
      min_index = 0
      possible_steps.each_with_index { |e, i| min_index = i if e.first < possible_steps[min_index].first }

      mst_distance += possible_steps[min_index].first

      current_point = possible_steps.delete_at(min_index).last

      0.upto(possible_steps.count - 1) do |i|
        current_point_distance = current_point.map.distance(current_point, possible_steps[i].last)
        possible_steps[i][0] = current_point_distance if current_point_distance < possible_steps[i].first
      end
    end

    mst_distance
  end
end

class PointTree
  attr_reader :best_node

  def initialize(tree_node)
    @tree = RBTree[]

    @best_node = tree_node
  end

  def pick_best_node
    return @best_node if @tree.empty?

    @best_node = @tree.first&.last&.shift

    @tree.shift if @tree.first&.last&.empty?

    @best_node
  end

  def create_child(tree_node, point)
    child = TreeNode.new(
        point,
        tree_node.distance + tree_node.point.map.distance(tree_node.point, point),
        tree_node.neighbours - [point],
        tree_node.current_path.clone
    )

    @tree[child.cost] = Array(@tree[child.cost]) << child

    child
  end

  def found_best_node?
    @best_node.neighbours.empty? and (@tree.empty? or (@best_node.cost <= @tree.first.first))
  end
end

# brute force algorithm

def path_length(map, path)
  first_point = path.first

  path.inject(
      last_visited: path.shift,
      distance: 0
  ) do |m, p|
    {
        last_visited: p,
        distance: m[:distance] + map.distance(m[:last_visited], p)
    }
  end[:distance] + map.distance(first_point, path.last)
end

def brute_force_algorithm(map)
  return map.points.clone if map.points.count < 3

  max_distance = 0

  map.points.each do |p|
    map.points.each do |q|
      next if p == q

      distance = map.distance(p, q)

      max_distance = distance if distance > max_distance
    end
  end

  shortest_path_found_length = max_distance * map.points.count
  shortest_path_found = nil

  start_time = Time.now

  # freeze last point from Array as a first point of the cycle
  map.points[0...-1].permutation.each do |p|
    break if Time.now - start_time > 60

    # eliminate the same reversed path
    next if p[0].object_id < p[-1].object_id

    p << map.points.last

    current_path_length = path_length(map, p.clone)

    if current_path_length < shortest_path_found_length
      shortest_path_found_length = current_path_length
      shortest_path_found = p
    end
  end

  shortest_path_found
end

# greedy algorithm

def greedy_algorithm(map)
  return [] if map.points.count.zero?

  points = map.points.clone
  current_point = points.shift
  path = [current_point]

  until points.empty?
    next_point = points.min_by { |p| map.distance(current_point, p) }

    current_point = next_point
    points.delete current_point
    path << current_point
  end

  path
end

# A* algorithm

def a_star_algorithm(map)
  return map.points if map.points.count < 3

  first_node = TreeNode.new(map.points.first,
                            0, map.points - [map.points.first],
                            []
  )
  tree = PointTree.new(first_node)

  until tree.found_best_node?
    current_best_node = tree.pick_best_node

    current_best_node.neighbours.each do |p|
      tree.create_child(current_best_node, p)
    end
  end

  tree.best_node.current_path
end
