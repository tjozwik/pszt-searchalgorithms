require 'codacy-coverage'
require 'simplecov'

SimpleCov.formatter = SimpleCov::Formatter::MultiFormatter.new([
                                                                   SimpleCov::Formatter::HTMLFormatter,
                                                                   Codacy::Formatter
                                                               ])

SimpleCov.start

require_relative '../algorithms'

describe FileManager do
  before :all do
    RSPEC_ROOT = File.dirname __FILE__
  end

  it 'imports points from file' do
    map = Map.new

    FileManager.new(map).import_points("#{RSPEC_ROOT}/fixtures/import_points_from_file.txt")

    expect(map.points[0].x).to eq 0
    expect(map.points[0].y).to eq 15
    expect(map.points[1].x).to eq 4
    expect(map.points[1].y).to eq 17
    expect(map.points[2].x).to eq 42
    expect(map.points[2].y).to eq 52
  end
end

describe Point do
  it 'contains point with correct co-ordinates' do
    point = Point.new(5, 42)

    expect(point.x).to eq 5
    expect(point.y).to eq 42
  end
end

describe Map do
  before :each do
    @map = Map.new
  end

  it 'adds points' do
    @map.add_point(5, 42)
    @map.add_point(7, 3)

    expect(@map.points.count).to eq 2
    expect(@map.points[0].x).to eq 5
    expect(@map.points[0].y).to eq 42
    expect(@map.points[1].x).to eq 7
    expect(@map.points[1].y).to eq 3
  end

  it 'calculates distance' do
    @map.add_point(0, 0)
    @map.add_point(0, 1)
    @map.add_point(1, 1)

    expect(@map.distance(@map.points[0], @map.points[1])).to eq 1
    expect(@map.distance(@map.points[0], @map.points[2])).to eq Math.sqrt(2)
  end
end

describe TreeNode do
  it 'creates valid object' do
    map = Map.new
    map.add_point(0, 0)
    map.add_point(4, 0)
    map.add_point(0, 3)
    map.add_point(4, 3)

    tree_node = TreeNode.new(map.points.first, 0, map.points[1..-1], [])

    expect(tree_node.point).to be map.points.first
    expect(tree_node.distance).to eq 0
    expect(tree_node.neighbours).to eq map.points[1..-1]
    expect(tree_node.current_path).to eq [map.points.first]
    expect(tree_node.cost).to eq 10
  end
end

describe PointTree do
  before :each do
    @map = Map.new

    @map.add_point(3, 0)
    @map.add_point(0, 0)
    @map.add_point(0, 4)

    @tree_node = TreeNode.new(
                            @map.points.first,
                            0,
                            @map.points[1..-1],
                            []
    )

    @point_tree = PointTree.new(@tree_node)
  end

  it 'creates valid object' do
    expect(@point_tree.best_node).to be @tree_node
  end

  it 'creates a child node' do
    child_node = @point_tree.create_child(@tree_node, @map.points.last)

    expect(child_node.point).to be @map.points.last
    expect(child_node.distance).to eq 5
    expect(child_node.neighbours).to eq [@map.points[1]]
    expect(child_node.current_path).to eq [@map.points.first, @map.points.last]
    expect(child_node.cost).to eq 12
  end

  it 'picks best node node when tree is empty' do
    expect(@point_tree.pick_best_node).to be @tree_node
  end

  it 'picks best node when tree is not empty' do
    child_node_1 = @point_tree.create_child(@tree_node, @map.points[1])
    child_node_2 = @point_tree.create_child(@tree_node, @map.points[2])

    # currently best node on tree
    expect(@point_tree.pick_best_node).to be child_node_1
    # best node after picking the previous one
    expect(@point_tree.pick_best_node).to be child_node_2
    # last best node treated as best node if tree is empty
    expect(@point_tree.pick_best_node).to be child_node_2
  end

  it 'returns false if best solution conditions are not matched' do
    expect(@point_tree.found_best_node?).to be_falsey
  end

  it 'returns true if best solution conditions are matched' do
    child_node_1 = @point_tree.create_child(@tree_node, @map.points[1])
    @point_tree.create_child(child_node_1, @map.points[2])

    3.times { @point_tree.pick_best_node }

    expect(@point_tree.found_best_node?).to be_truthy
  end
end

describe 'brute force algorithm' do
  before :each do
    @map = Map.new
  end

  it 'calculates shortest path' do
    @map.add_point(1, 0)
    @map.add_point(2, 0)
    @map.add_point(0, 0)
    @map.add_point(3, 0)

    expect(path_length(@map, brute_force_algorithm(@map))).to eq 6
  end

  it 'handles empty map' do
    expect(brute_force_algorithm(@map)).to eq []
  end

  it 'handles map with one point' do
    @map.add_point(5, 17)

    expect(brute_force_algorithm(@map)).to eq [@map.points[0]]
  end
end

describe 'path distance calculation' do
  it 'returns path distance' do
    map = Map.new

    map.add_point(0, 1)
    map.add_point(0, 2)
    map.add_point(0, 3)

    expect(path_length(map, [map.points[0], map.points[1], map.points[2]])).to eq 4
    expect(path_length(map, [map.points[1], map.points[2], map.points[0]])).to eq 4
  end
end

describe 'greedy algorithm' do
  before :each do
    @map = Map.new
  end

  it 'calculates correct path' do
    @map.add_point(0, 1)
    @map.add_point(0, -5)
    @map.add_point(0, 3)
    @map.add_point(0, 0)

    expect(path_length(@map, greedy_algorithm(@map))).to eq 18
  end

  it 'handles empty map' do
    expect(greedy_algorithm(@map)).to eq []
  end

  it 'handles map with one point' do
    @map.add_point(0, 0)

    expect(greedy_algorithm(@map)).to eq [@map.points.first]
  end
end

describe 'a star algorithm' do
  before :each do
    @map = Map.new
  end

  it 'handles empty map' do
    expect(a_star_algorithm(@map)).to eq []
  end

  it 'handles map with one point' do
    @map.add_point(42, 15)

    expect(a_star_algorithm(@map)).to eq @map.points
  end

  it 'handles map with two points' do
    @map.add_point(0, 0)
    @map.add_point(5, 0)

    expect(path_length(@map, a_star_algorithm(@map))).to eq 10
  end

  it 'returns correct result' do
    @map.add_point(0, 0)
    @map.add_point(0, 3)
    @map.add_point(4, 0)
    @map.add_point(4, 3)

    expect(path_length(@map, a_star_algorithm(@map))).to eq 14
  end
end