# PSZT - Search Algorithms

Project compares a couple of algorithms used to find solution for [Travelling Salesman Problem (TSP)](https://en.wikipedia.org/wiki/Travelling_salesman_problem).

Implemented algorithms:

- Brute force
- Greedy
- A*

## Installation

Use the Ruby Version Manager (rvm) to install required Ruby interpreter:

```bash
gpg2 --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3 7D2BAF1CF37B13E2069D6956105BD0E739499BDB
\curl -sSL https://get.rvm.io | bash -s stable
```

Install project dependencies:

```bash
bundle
```

## Usage

Prepare input file with points' coordinates - one point in each row. 

Example for points (1, 0), (5, 7) and (3, 10):

```bash
1 0
5 7
3 10
```
Run program using command below and follow displayed instructions:

```bash
ruby project.rb
```

If you want to use our algorithms implementation in other Ruby-based projects, include them using:

```ruby
require_relative 'algorithms'
```

## Authors
- Tomasz Jóźwik
- Tomasz Mazur
