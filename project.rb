require_relative 'algorithms'

def console_handler
  print 'Input file name: '
  input = gets.chomp
  unless File.exists? input
    print 'File does not exist'
    return
  end

  print 'Output file name: '
  output = gets.chomp

  map = Map.new
  manager = FileManager.new(map)
  manager.import_points(input)

  puts '''Choose algorithm:
  1 - Brute force
  2 - Greedy
  3 - A*'''

  case gets.chomp.to_i
  when 1
    manager.export_path(brute_force_algorithm(map), output)
  when 2
    manager.export_path(greedy_algorithm(map), output)
  when 3
    manager.export_path(a_star_algorithm(map), output)
  end
end

console_handler
